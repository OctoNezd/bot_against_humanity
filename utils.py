import subprocess
import io
IMAGEMAGICK_CREATE_STICKER = "magick -verbose sticker.png -background none -size 448x420 -gravity NorthWest -fill white -font Fira-Sans-ExtraBold caption:@- -extent 512x512 -gravity NorthWest -geometry +32+32 -composite webp:-"
def create_sticker(text):
    imagick, errors = subprocess.Popen(IMAGEMAGICK_CREATE_STICKER, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE).communicate(text.encode("utf-8"))
    sticker = io.BytesIO(imagick)
    sticker.seek(0)
    return sticker

if __name__ == '__main__':
    with open("test.webp", 'wb') as f:
        f.write(create_sticker("wow such a long test to make sure it will overflow" + " and stuff" * 20).read())