from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
import cah_strings
import telegram
import utils
import html
import cards
import settings
import time
import logging
LOGGER = logging.getLogger("BAH")
NL = "\n"
updater = Updater(settings.TOKEN, **settings.UPDATER_KWARGS)
me = updater.bot.getMe()
PM_ME = telegram.InlineKeyboardMarkup(
    [[telegram.InlineKeyboardButton(text="PM bot", url="https://t.me/" + me.username)]])


def delete_game(game):
    cards.Player.delete().where(cards.Player.game == game).execute()
    game.delete_instance()


def update_join_caption(message, game):
    already_joined = []
    for user in cards.Player.select().where(cards.Player.game == game):
        already_joined.append((
            f'<a href="tg://user?id={str(user.telegram_id)}">'
            f"""{html.escape(str(user.name))}</a>"""
        ))
    updater.bot.edit_message_text(
        message_id=game.join_mid,
        chat_id=game.chat_id,
        text=(
            "<b>Join the game of Bot Against Humanity!</b>\n"
            "These people already joined, so why won't you?\n"
            f'{", ".join(already_joined)}'
        ),
        parse_mode="HTML",
        reply_markup=telegram.InlineKeyboardMarkup([
            [
                telegram.InlineKeyboardButton(
                    url=f"https://t.me/{me.username}?start=join_{game.chat_id}", text="Join"),
                telegram.InlineKeyboardButton(
                    callback_data=f"start", text="Start game")
            ]
        ])
    )


def create_game(bot, update):
    if update.message.chat.type not in ["channel", "private"]:
        try:
            game = cards.Game.select().where(
                str(update.message.chat.id) == cards.Game.chat_id).get()
        except cards.DoesNotExist:
            message = update.message.reply_text(
                "Please wait, creating your game...")
            if update.message.chat.username is None:
                try:
                    invite_link = bot.exportChatInviteLink(
                        update.message.chat.id)
                except telegram.error.BadRequest:
                    invite_link = ""
            else:
                invite_link = "https://t.me/" + update.message.chat.username
            game = cards.Game.create(chat_id=str(
                update.message.chat.id), join_mid=str(message.message_id), invite_link=invite_link, chat_name=update.message.chat.title)
            player = cards.Player.create(name=update.message.from_user.first_name,
                                         telegram_id=str(update.message.from_user.id), game=game)
            update_join_caption(message, game)
            try:
                bot.pin_chat_message(
                    update.message.chat.id, message.message_id, disable_notification=False)
            except telegram.error.TelegramError:
                pass
        else:
            bot.sendMessage(chat_id=update.message.chat.id,
                            reply_to_message_id=game.join_mid, text="<b>Game is already running!</b>", parse_mode="HTML")
    else:
        update.message.reply_text("This command only works in groups!")


def select_czar(bot, game):
    czar = cards.Player.select().where(
        cards.Player.game == game).order_by(cards.fn.Random()).get()
    bot.sendMessage(chat_id=czar.telegram_id,
                    text="<b>You are czar of this round!</b>", parse_mode="HTML")
    return czar.telegram_id, f'<a href="tg://user?id={czar.telegram_id}">{html.escape(czar.name)}</a> is czar of this round!'


def start_round(bot, game, mes_prepend="", run_now=False):
    top = cards.Player.select().where(
        cards.Player.game == game).order_by(cards.Player.score.desc())
    if top[0].score < 5:
        game_chatid = game.chat_id


        def new_round(*_, **__):
            game = cards.Game.select().where(cards.Game.chat_id == game_chatid).get()
            top = cards.Player.select().where(
                cards.Player.game == game).order_by(cards.Player.score.desc())
            current_top = []
            for player in top:
                current_top.append(
                    f"{player.name} - {player.score} Awesome Points")
            LOGGER.info("Starting round")
            players = cards.Player.select().where(cards.Player.game == game)
            czar_id, czar_msg = select_czar(bot, game)
            black_card = cards.Card.select().where(
                cards.Card.prompt == True).order_by(cards.fn.Random()).get().text
            game.black_card = black_card
            game.table_cards = {}
            game.table_cards_nf = {}
            game.czar = czar_id
            bc_count = black_card.count("{}")
            game.save()
            msg = bot.edit_message_text(chat_id=game.chat_id,
                                        message_id=game.join_mid,
                                        text=(f"{mes_prepend}Current players top:\n<i>{html.escape(NL.join(current_top))}</i>\n"
                                              f"<b>The black card of this round is:</b>\n<i>{html.escape(black_card.replace('{}', '____'))}</i>"
                                              f"\n{'='*10}\n{czar_msg}"),
                                        parse_mode="HTML")
            bot.sendMessage(chat_id=game.chat_id, text="<b>ROUND HAS BEGUN</b>\n<i>Check your PMs</i>\n" +
                            czar_msg, parse_mode="HTML", reply_to_message_id=game.join_mid, reply_markup=PM_ME)
            if run_now:
                start_notice.delete()
            try:
                bot.pin_chat_message(
                    msg.chat.id, msg.message_id, disable_notification=True)
            except telegram.error.TelegramError:
                pass

            give_cards(bot, players, czar_id, black_card)
        if run_now:
            start_notice = bot.sendMessage(
                chat_id=game_chatid, text="<i>Round will start in 5 seconds...</i>", parse_mode="HTML", reply_to_message_id=game.join_mid)
            updater.job_queue.run_once(new_round, 5)
        else:
            new_round()
    else:
        bot.sendMessage(chat_id=game.chat_id,
                        text=f'<a href="tg://user?id={top[0].telegram_id}">{top[0].name}</a> wins!\nType /new_game to start new game', parse_mode="HTML")
        delete_game(game)


def give_cards(bot, players, czar, bc):
    for player in players:
        if not player.telegram_id == czar:
            player_cards = []
            while len(player.cards) != 10:
                rand_card = cards.Card.select().where(
                    cards.Card.prompt == False).order_by(cards.fn.Random()).get().id
                player.cards.append(rand_card)
            player.save()
            for card in player.cards:
                player_cards.append([cards.Card.select().where(
                    cards.Card.id == card).get().text, card])
            LOGGER.debug(player_cards)
            kbd = []
            for card, cid in player_cards:
                kbd.append([telegram.InlineKeyboardButton(
                    callback_data="pick_%s" % cid, text=card)])
            bot.sendMessage(chat_id=player.telegram_id,
                            text="<i>%s</i>\n<b>Pick %s cards:</b>\n" % (html.escape(bc.replace("{}", "____")), bc.count("{}")), parse_mode="HTML", reply_markup=telegram.InlineKeyboardMarkup(kbd))
            time.sleep(0.2)


def generate_black_card(game, player_id):
    cards_text = []
    for card in game.table_cards_nf[str(player_id)]:
        cards_text.append(cards.Card.select().where(
            cards.Card.id == card).get().text.strip("."))
    LOGGER.debug("%s | %s", cards_text, game.black_card)
    return game.black_card.format(*tuple(cards_text))


def czar_vote_start(bot, game):
    LOGGER.info("Starting czar vote")
    kbd = []
    versions = []
    for number, result in enumerate(game.table_cards.items()):
        number = number + 1
        player, result = result
        LOGGER.info(result)
        LOGGER.info(player)
        kbd.append([telegram.InlineKeyboardButton(
            text=number, callback_data="czar_" + str(player))])
        versions.append(f"{number}. {html.escape(result)}")
    kbd = telegram.InlineKeyboardMarkup(kbd)
    bot.sendMessage(chat_id=game.czar,
                    text="<b>Pick the best version of black card:</b><i>\n" + "\n".join(versions) + "</i>", reply_markup=kbd, parse_mode="HTML")


def pick_cards(bot, update):
    try:
        player = cards.Player.select().where(str(update.callback_query.from_user.id)
                                             == cards.Player.telegram_id).get()
    except cards.DoesNotExist:
        update.callback_query.answer(
            "Failed to pick card: you are not playing", alert=True)
    else:
        game = player.game
        if update.callback_query.from_user.id == game.czar:
            update.callback_query.answer(
                "Failed to pick card: you are czar", alert=True)
        else:
            card = int(update.callback_query.data.split("_")[1])
            if card in player.cards:
                bc_count = game.black_card.count("{}")
                if not str(player.id) in game.table_cards_nf:
                    game.table_cards_nf[str(player.id)] = []
                if len(game.table_cards_nf[str(player.id)]) >= bc_count:
                    update.callback_query.answer(
                        "Failed to pick card: you already picked %s cards" % bc_count, alert=True)
                    return
                game.table_cards_nf[str(player.id)].append(card)
                player.cards.remove(card)
                player.save()
                game.save()
                if len(game.table_cards_nf[str(player.id)]) == bc_count:
                    game.table_cards[str(
                        player.id)] = generate_black_card(game, player.id)
                    game.save()
                    if game.invite_link != "":
                        back_to_chat = telegram.InlineKeyboardMarkup(
                            [[telegram.InlineKeyboardButton(text="Back to chat", url=game.invite_link)]])
                    else:
                        back_to_chat = None
                    update.callback_query.message.edit_text(
                        text="<i>Your white+black card combination:</i>\n" + generate_black_card(game, player.id), parse_mode="HTML", reply_markup=back_to_chat)
                    players = len(cards.Player.select().where(
                        cards.Player.game == game)) - 1
                    bot.sendMessage(
                        chat_id=game.chat_id, text=f'<a href="tg://user?id={player.telegram_id}">{html.escape(player.name)}</a> had selected their white card(s) ({len(game.table_cards)} out of {players})', parse_mode="HTML")
                    if players == len(game.table_cards):
                        bot.sendMessage(
                            chat_id=game.chat_id, text=("<b>Everyone selected their white card(s)!</b>"
                                                        "\n<i>Now waiting for czar to choose best combination/card</i>"
                                                        "\n<b>Current options:</b>\n"
                                                        "<i>" +
                                                        html.escape(
                                                            "\n-------\n".join(game.table_cards.values())) + "</i>"
                                                        ), parse_mode="HTML")
                        czar_vote_start(bot, game)
                else:
                    update.callback_query.answer(
                        "%s left to pick" % (bc_count - len(game.table_cards_nf[str(player.id)])), alert=True)
            else:
                update.callback_query.answer(
                    "Failed to pick card: you don't have this card", alert=True)


def czar_vote(bot, update):
    try:
        player = cards.Player.select().where(str(update.callback_query.from_user.id)
                                             == cards.Player.telegram_id).get()
    except cards.DoesNotExist:
        update.callback_query.answer(
            "Failed to pick black card: you are not czar", alert=True)
    else:
        game = player.game
        wid = update.callback_query.data.split("_")[1]
        winner = cards.Player.select().where(cards.Player.id == int(wid)).get()
        winner.score += 1
        winner.save()
        if game.invite_link != "":
            back_to_chat = telegram.InlineKeyboardMarkup(
                [[telegram.InlineKeyboardButton(text="Back to chat", url=game.invite_link)]])
        else:
            back_to_chat = None
        update.callback_query.message.edit_text(
            f"You picked <i>{html.escape(game.table_cards[wid])}</i>", parse_mode="HTML", reply_markup=back_to_chat)
        msg = bot.sendMessage(chat_id=game.chat_id, text=(
            f'<a href="tg://user?id={winner.telegram_id}">{html.escape(winner.name)}</a>'
            f' <b>won</b> this round (Current Awesome Points: {winner.score})\n'
            f"<i>{html.escape(game.table_cards[wid])}</i>\n{'='*10}\n"),
            parse_mode="HTML"
        )
        msg.reply_sticker(utils.create_sticker(game.table_cards[wid]))
        start_round(bot, game, mes_prepend=(
            f'<a href="tg://user?id={winner.telegram_id}">{html.escape(winner.name)}</a>'
            f' <b>won</b> this round (Current Awesome Points: {winner.score})\n'
            f"<i>{html.escape(game.table_cards[wid])}</i>\n{'='*10}\n"))


def start_game(bot, update):
    try:
        game = cards.Game.select().where(
            str(update.callback_query.message.chat.id) == cards.Game.chat_id).get()
    except cards.DoesNotExist:
        update.callback_query.answer(
            "Failed to start game: game doesn't exist", alert=True)
    else:
        if game.state == cards.STATE_GAME_WAITING:
            players = cards.Player.select().where(cards.Player.game == game)
            if len(players) > 2:
                update.callback_query.answer("Starting game...")
                game.state = cards.STATE_GAME_RUNNING
                game.save()
                start_round(bot, game)
            else:
                update.callback_query.answer("Not enough players!")
        else:
            update.callback_query.answer(
                "Failed to start game: game already running", alert=True)


def start_command(bot, update):
    if len(update.message.text.split(" ")) > 1 and update.message.text.split(" ")[1].startswith("join_"):
        try:
            game = cards.Game.select().where(
                str(update.message.text.split(" ")[1].lstrip("join_")) == cards.Game.chat_id).get()
        except cards.DoesNotExist:
            update.message.reply_text(
                "Failed to join: game doesn't exist")
        else:
            try:
                player = cards.Player.select().where(str(update.message.from_user.id)
                                                     == cards.Player.telegram_id).get()
            except cards.DoesNotExist:
                if game.state == cards.STATE_GAME_WAITING:
                    player = cards.Player.create(name=update.message.from_user.first_name,
                                                 telegram_id=update.message.from_user.id, game=game)
                    update.message.reply_text(
                        f'Joined game at <a href="{game.invite_link}">{html.escape(game.chat_name)}</a>!', parse_mode="HTML")
                    update_join_caption(game.join_mid, game)
                    LOGGER.info("Player %s joins", player.id)
                else:
                    update.message.reply_text(
                        "Failed to join: game is already running")
            else:
                LOGGER.error("Player %s is already playing!", player.id)
                update.message.reply_text(
                    "Failed to join: you are already playing")
    else:
        update.message.reply_text(cah_strings.START, parse_mode="HTML")


def pm_only(func):
    def handle(bot, update):
        if update.message and update.message.chat.type == update.message.chat.PRIVATE:
            return func(bot, update)
        else:
            return update.message.reply_text("This command is PM-only", reply_markup=PM_ME)
    return handle


@pm_only
def help_cmd(bot, update):
    update.message.reply_text(cah_strings.HELP, parse_mode="HTML")


def rules(bot, update):
    update.message.reply_text(cah_strings.RULES, parse_mode="HTML")


@pm_only
def about(bot, update):
    update.message.reply_text(cah_strings.ABOUT, parse_mode="HTML")


@pm_only
def rooms(bot, update):
    kbd = []
    for chat in settings.CHATS:
        chat = bot.getChat(chat)
        kbd.append([telegram.InlineKeyboardButton(
            url=bot.exportChatInviteLink(
                        chat.id), text=chat.title)])
    update.message.reply_text("Here you go. Join whatever suits your language.",
                              reply_markup=telegram.InlineKeyboardMarkup(kbd))


def ping(bot, update):
    update.message.reply_text("Pong.")


def fstop(bot, update):
    if update.message.chat.type != "private":
        try:
            game = cards.Game.select().where(update.message.chat.id == cards.Game.chat_id).get()
        except cards.DoesNotExist:
            update.message.reply_text(
                "Failed to force stop: game doesn't exist")
        else:
            for user in update.message.chat.get_administrators():
                if user.user.id == update.message.from_user.id:
                    delete_game(game)
                    return update.message.reply_text("<i>Force stopped game.</i>", parse_mode="HTML")
            return update.message.reply_text("You are not admin.")
    else:
        update.message.reply_text("You are not in game.")


def leave_game(bot, update):
    try:
        player = cards.Player.select().where(str(update.message.from_user.id)
                                             == cards.Player.telegram_id).get()
    except cards.DoesNotExist:
        update.message.reply_text(
            "Failed to leave game: you are not playing", alert=True)
    else:
        game = player.game
        bot.sendMessage(chat_id=game.chat_id, text=f'Player <a href="tg://user?id={player.telegram_id}">{html.escape(player.name)}</a> left the game. Round will be restarted.', parse_mode="HTML")
        player.delete_instance()
        update.message.reply_text("You left the game.")
        start_round(bot, game)


def main():
    commands = [[["bah_play"], create_game],
                [["start"], start_command],
                [["bah_rules"], rules],
                [["about"], about],
                [["rooms"], rooms],
                [["bah_ping"], ping],
                [["bah_fstop"], fstop],
                [["bah_leave"], leave_game],
                [["help"], help_cmd]]
    for command, function in commands:
        updater.dispatcher.add_handler(CommandHandler(command, function))

    callbacks = [
        ["start", start_game],
        ["pick_.*", pick_cards],
        ["czar_.*", czar_vote]
    ]
    for callback, function in callbacks:
        updater.dispatcher.add_handler(
            CallbackQueryHandler(pattern=callback, callback=function))
    LOGGER.info("Logged in as @%s", updater.bot.getMe().username)
    LOGGER.info("Starting poll")
    updater.start_polling(clean=True)
    updater.idle()


if __name__ == '__main__':
    main()
