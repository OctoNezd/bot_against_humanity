import settings
START = (
    'Hi! I am Bot Against Humanity, a bot that mimics experience of the amazing party game for horrible people called <a href="https://cardsagainsthumanity.com/">Cards Against Humanity</a>.\n'
    'Please notice that some of stuff may be offensive (and it is <i>designed</i> to be offensive) to some or all people. By entering the game you agree to that.\n'
    'Here are some basic commands you will probably need in your "journey":\n'
    '/start - this command (duh)\n'
    '/bah_play - starts new game\n'
    '/bah_rules - game rules\n'
    '/rooms - command for people without friends who want to play game with random people (Works only in PM)\n'
    'Other commands are available in /help command. (Works only in PM to prevent flood)'
)
HELP = (
    '<b>Commands:</b>\n'
    '/bah_play - starts new game. Anyone can run it and anyone can click start game button. No, I won\' change this behaviour.\n'
    '/bah_fstop - force stops current game. This command is available <i>only</i> to admins\n'
    '/bah_leave - makes you leave game\n'
    '/bah_rules - sends you rules of Bot Against Humanity\n'
    '/bah_ping - ping? pong.\n'
    'PM exclusive commands:\n'
    '/rooms - sends you our very cool list of chats with bot so you can play if you have no friends that are willing to play\n'
    '/help - duh\n'
    '/about - credits and stuff\n'
    '<b>FAQ:</b>\n'
    'Q: <i>Your bot sucks! Implement X and Y and Z!</i>\n'
    f'<i>Submit your "cool ideas" to</i> <a href="{settings.SUPPORT_INVITE_LINK}">our support chat</a>\n'
    'Q: <i>Can we get more decks and not include Canadian/UK decks by default?</i>\n'
    'A: Soon™️'
)
RULES = (
    "Okay, so basically:\n"
    "i̶ ̶a̶m̶ ̶m̶o̶n̶k̶y̶\n"
    "Each player draws 10 white cards.\n"
    'Each player picks one or two or even sometimes three white cards which will fill the gaps in "randomly" selected black card.\n'
    'One player is "randomly" (Python random sucks, maybe I will improve it soon ™️) selected as czar and picks the funniest play\n'
    'The first one to get 5 points wins\n'
    'Also I implemented sticker generation of winned play, which you can save into group\'s sticker pack using my amazing and innovative bot @aigis_bot'
)

ABOUT = (
    'PROUDLY RUNNING ON PYTHON3 - I WILL NOT MAKE A JS/PHP/WHATEVER VERSION, FUCK OFF\n'
    'STICKER GENERATION POWERED BY "MOST FAST" TOOL THAT I "KNOW" HOW TO USE CALLED IMAGEMAGICK\n'
    'Created by @OctoNezd\n'
    'Thanks to the following people for helping me test this bot:\n'
    '@NotVolca and @notcake, @SquidSan, @gershik, @anarchycell, @medianik, @handlerug, @evchk'
)
