from tqdm import tqdm
import re
import csv
import cards
import logging

LOGGER = logging.getLogger("Deck generator")


def generate_main_deck():
    cards.wipe()
    unsaved = []
    with open("maindeck.csv", encoding="ascii", errors="ignore") as f:
        deckreader = list(csv.reader(f))
        for row in tqdm(deckreader, unit="rows"):
            if row[0] == "Set":
                current_deck = cards.Deck.create(name=row[1])
                current_deck.save()
            elif row[0] == "Prompt":
                fxd = re.sub(r'_{1,}', "{}", row[1]).strip()
                if "{}" in fxd:
                    unsaved.append(dict(text=fxd, prompt=True, deck=current_deck))
            elif row[0] == "Response":
                unsaved.append(dict(
                    text=row[1], prompt=False, deck=current_deck))
    with cards.db.atomic():
        for batch in tqdm(list(cards.chunked(unsaved, 100)), unit="chunks"):
            cards.Card.insert_many(batch).execute()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    generate_main_deck()
