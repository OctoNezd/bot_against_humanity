# # Colorful logging with coloredlogs
# import coloredlogs
# coloredlogs.install(
#     level='INFO', fmt="%(asctime)s %(name)s %(levelname)s %(message)s")
# # Basic python logging without colors
# import logging
# logging.basicConfig(level=logging.INFO)
# Telegram token.
TOKEN = ""
# Put chat ids for /rooms command here
CHATS = []
# Link to support chat in /help
SUPPORT_INVITE_LINK = ""
# Kwargs for python-telegram-bot updater class
UPDATER_KWARGS = {}