# Bot Against Humanity

## Installation

0. Use python 3. I think any version should do the trick. Don't use python2.7, it's 2018

1. Install requirements

```
pip install peewee python-telegram-bot tqdm
```

Download & install imagemagick 7+

Download & install (amazing and cool and good) Fira Sans font

1.1 Install optional requirement `colored-logs` for colored logging

2. `cp settings_example.py settings.py && subl settings.py` - write your settings

3. Go to https://docs.google.com/spreadsheets/d/1lsy7lIwBe-DWOi2PALZPf5DgXHx9MEvKfRw1GaWQkzg/edit?usp=sharing#gid=13 - export the CAH Main Deck as csv with name maindeck.csv

4. Build deck data - `python3 deck_add.py`

5. Run bot

`python3 bot.py`

