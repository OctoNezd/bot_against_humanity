from peewee import *
from playhouse.sqlite_ext import *
db = SqliteDatabase("cards.db")
STATE_GAME_RUNNING = "RUNNING"
STATE_GAME_WAITING = "WAITING"


class BaseModel(Model):
    class Meta:
        database = db


class Deck(BaseModel):
    name = CharField(unique=True)


class Card(BaseModel):
    text = CharField()
    prompt = BooleanField()
    deck = ForeignKeyField(Deck, backref="cards")


class Game(BaseModel):
    chat_id = CharField(unique=True)
    join_mid = CharField()
    state = CharField(default=STATE_GAME_WAITING)
    czar = CharField(default="0")
    table_cards_nf = JSONField(default={})
    table_cards = JSONField(default={})
    black_card = CharField(default="0")
    chat_name = CharField()
    invite_link = CharField()


class Player(BaseModel):
    name = CharField()
    telegram_id = CharField(unique=True)
    score = IntegerField(default=0)
    game = ForeignKeyField(Game, backref="+")
    cards = JSONField(default=[])


db.connect()


def wipe():
    db.drop_tables([Deck, Card])
    db.create_tables([Deck, Card])


db.drop_tables([Game, Player])
db.create_tables([Game, Player])
